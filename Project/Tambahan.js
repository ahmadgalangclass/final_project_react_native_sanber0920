import React, {Component} from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';
import Data from './tambahan.json';
import {connect} from 'react-redux';

class Tambahan extends Component{
    renderItem = ({item}) => {
        return(
            <View>
                <Image style={styles.gambar} source={{uri: item.image}} />
                    <View style={styles.menu}>
                        <Text style={styles.namaTambahan}>{item.name}</Text>
                        <Text style={styles.harga}>{item.harga}</Text>
                    </View>
                    <View style={styles.btnContainer}>
                        <TouchableOpacity onPress={this.props.handleMinus}>
                            <Icon style={styles.btn} name="minus-a" size={20} />
                        </TouchableOpacity>
                        <Text style={styles.btn}>{item.order}</Text>
                        <TouchableOpacity onPress={this.props.handlePlus}>
                            <Icon style={styles.btn} name="plus-a" size={20} />
                        </TouchableOpacity>
                    </View>
            </View>
        )
    }
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.barAtas}>
                    <Text style={styles.nama}>Pesan Apa?</Text>
                </View>
                <View style={styles.tab}>
                    <TouchableOpacity style={styles.btnKopi} onPress={() => this.props.navigation.navigate('Kopi')}>
                        <Text style={styles.textBar}>Rasa Apa?</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.btnTambahan}>
                        <Text style={styles.textTambahan}>Tambahan</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.mainContainer}>
                    <FlatList
                    data={Data.items}
                    renderItem={this.renderItem}
                    keyExtractor={(item) => item.id}
                    />
                </View>
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="home" size={25} style={{color: '#27AE60'}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Belanja')}>
                        <Text style={{color: '#FFFFFF'}}>{this.props.order}</Text>
                        <Icon name="shopping-basket" size={25} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Tentang')}>
                        <Icon name="person" size={25} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        order: state.totalOrder
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        handlePlus: () => dispatch({type: 'PLUS_ORDER'}),
        handleMinus: () => dispatch({type: 'MINUS_ORDER'})
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Tambahan);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    barAtas: {
        height: 65,
        backgroundColor: '#4F4F4F',
        alignItems: 'center',
        justifyContent: 'center'
    },
    nama: {
        fontFamily: 'Roboto',
        fontSize: 40,
        fontWeight: 'bold',
        color: '#F2F2F2',
        lineHeight: 56
    },
    tab: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        height: 20
    },
    btnTambahan: {
        // backgroundColor: '#ffffff',
        height: 20,
        width: 180
    },
    textTambahan: {
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 'bold',
        color: '#10202F',
        lineHeight: 16,
        textAlign: 'center'
    },
    btnKopi: {
        backgroundColor: '#828282',
        height: 20,
        width: 180,
        elevation : 4
    },
    textBar: {
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 'bold',
        color: '#ffffff',
        lineHeight: 18,
        textAlign: 'center'
    },
    mainContainer: {
        flexDirection: 'column',
        paddingVertical: 25,
        flex: 1
    },
    gambar: {
        width: 237,
        height: 183,
        alignSelf: 'center',
        marginTop: 10
    },
    menu: {
        paddingHorizontal: 60,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    namaTambahan: {
        fontFamily: 'Roboto',
        fontSize: 14,
        fontWeight: 'bold',
        color: '#000000',
        lineHeight: 16,
        marginTop: 5
    },
    harga: {
        fontFamily: 'Roboto',
        fontSize: 12,
        fontWeight: 'bold',
        color: '#000000',
        lineHeight: 14,
        marginTop: 6
    },
    btnContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#219653',
        height: 30,
        width: 120,
        alignSelf: 'center',
        borderRadius: 25,
        paddingHorizontal: 10,
        paddingVertical: 4,
        marginVertical: 15,
        elevation : 4
    },
    btn: {
        color: '#ffffff',
        alignSelf: 'center',
    },
    tabBar: {
        backgroundColor: '#333333',
        height: 60,
        borderTopWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        elevation: 4
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})