import React, {Component} from 'react';
import { View, StyleSheet, Image, Text, TouchableOpacity, ScrollView, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/Fontisto';
import {connect} from 'react-redux';

class About extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.barAtas}>
                    <Text style={styles.nama}>KopiPesen!</Text>
                </View>
                <ScrollView>
                <View style={styles.perkenalan}>
                    <Image style={styles.gambar} source={require('./images/Gyro.jpg')} />
                    <Text style={styles.name}>Ahmad Galang Satria</Text>
                    <Text style={styles.ket}>React Native</Text>
                </View>
                <View style={styles.boxA}>
                    <View style={styles.contact}>
                        <View style={styles.info}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://api.instagram.com/langgahmad')}>
                                <Icon style={{margin:10, color: '#FFFFFF'}} name="instagram" size={40} />
                            </TouchableOpacity>
                            <Text style={styles.username}>@langgahmad</Text>
                        </View>
                        <View style={styles.info}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://api.whatsapp.com/send?phone=6285790735855&text=Halo')}>
                                <Icon style={{margin: 10, color: '#24FF00'}} name="whatsapp" size={40} />
                            </TouchableOpacity>
                            <Text style={styles.username}>Ahmad</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.boxA}>
                    <View style={styles.contact}>
                        <View style={styles.info}>
                            <TouchableOpacity onPress={() => Linking.openURL('https://gitlab.com/ahmadgalangclass')}>
                                <Icon style={{margin:10, color: '#EE9623'}} name="gitlab" size={40} />
                            </TouchableOpacity>
                            <Text style={styles.username}>@ahmadgalangclass</Text>
                        </View>
                        <View style={styles.info}>
                            <TouchableOpacity onPress={() => Linking.openURL('facebook.com/gyozpada')}>
                                <Icon style={{margin: 10, color: '#80AFE8'}} name="facebook" size={40} />
                            </TouchableOpacity>
                            <Text style={styles.username}>facebook.com/gyozpada</Text>
                        </View>
                    </View>
                </View>
                </ScrollView>
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Kopi')}>
                        <Icon name="home" size={25} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem} onPress={() => this.props.navigation.navigate('Belanja')}>
                        <Text style={{color: '#FFFFFF'}}>{this.props.order}</Text>
                        <Icon name="shopping-basket" size={25} style={{color: '#FFFFFF'}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="person" size={25} style={{color: '#27AE60'}} />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const mapStateToProps = (state) => {
    return{
        order: state.totalOrder
    }
}
export default connect(mapStateToProps)(About);

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    barAtas: {
        height: 65,
        backgroundColor: '#4F4F4F',
        alignItems: 'center',
        justifyContent: 'center'
    },
    nama: {
        fontFamily: 'Roboto',
        fontSize: 40,
        fontWeight: 'bold',
        color: '#F2F2F2',
        lineHeight: 56
    },
    perkenalan: {
        margin: 30,
        alignItems: 'center'
    },
    gambar: {
        width: 165,
        height: 165,
        borderRadius: 50
    },
    name: {
        fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight: '500',
        color: '#000000',
        lineHeight: 28,
        marginTop: 28
    },
    ket: {
        fontFamily: 'Roboto',
        fontSize: 18,
        fontWeight: '500',
        color: '#10202F',
        lineHeight: 21
    },
    boxA: {
        height: 120,
        backgroundColor: '#56CCF2',
        borderRadius: 25,
        marginHorizontal: 38,
        marginBottom: 15,
        elevation : 5
    },
    contact: {
        height: 100,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center'
    },
    info: {
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    username: {
        fontFamily: 'Roboto',
        fontSize: 10,
        fontWeight: '500',
        color: '#F2F2F2',
        lineHeight: 12
    },
    tabBar: {
        backgroundColor: '#333333',
        height: 60,
        borderTopWidth: 0.5,
        flexDirection: 'row',
        justifyContent: 'space-around',
        elevation: 4
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    }
})